#!/bin/bash

cd /app

# Place the DB from prod (yesterday's backup).
drush sqlq --file=/app/db_backups/db-prod-backup-latest.sql.gz

# Deploy any changes on the code.
drush deploy

# Sanitize the DB.
drush -y sql:sanitize

# Show files from prod.
drush -y en stage_file_proxy

# And give the user running the script a link.
drush uli
