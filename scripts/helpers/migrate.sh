#!/usr/bin/env bash

# Begin with a status overview.
drush migrate:status --group=base,group,l10n

# Migrate base.
drush migrate:import l10n_server_language
drush migrate:import localize_d7_file
drush migrate:import localize_d7_user
drush migrate:import localize_d7_taxonomy_term_vocabulary_news_tags
drush migrate:import localize_d7_node_complete_news
drush migrate:import localize_d7_node_complete_page

# Migrate group.
drush migrate:import l10n_server_group
drush migrate:import localize_d7_node_complete_poll
drush migrate:import localize_d7_taxonomy_term_vocabulary_group_tags
drush migrate:import localize_d7_node_complete_story
drush migrate:import localize_d7_node_complete_wiki

# Migrate localization tables.
drush migrate:import l10n_server_project
drush migrate:import l10n_server_release
drush migrate:import l10n_server_file
drush migrate:import l10n_server_line
drush migrate:import l10n_server_string
drush migrate:import l10n_server_status_flag
drush migrate:import l10n_server_error
drush migrate:import l10n_server_translation
drush migrate:import l10n_server_translation_history
