#!/bin/bash

# IMPORTANT: Tools required
# * ddev: https://ddev.readthedocs.io/en/latest/users/install/ddev-installation
# * kubectl: https://gitlab.com/groups/drupal-infrastructure/-/wikis/kubectl-basics:-communicate-to-rancher-pods-from-local

ddev start
ddev composer install

# Bring DB from prod (or from a sanitized version of prod).
# Backups are taken each day at 00:00 so we should always have a copy of today available.

PREVIOUS_CONTEXT=$(kubectl config current-context)

kubectl config use-context rancher_prod
kubectl cp api-prod/"$(kubectl get pods -n api-prod --field-selector=status.phase=Running -o custom-columns=':metadata.name' --no-headers 2>&1 | head -n 1)":/app/db_backups/db-backup-latest.sql.gz api-prod-db.sql.gz

kubectl config use-context $PREVIOUS_CONTEXT

# Import DB into local instance.
ddev import-db --file=api-prod-db.sql.gz
rm api-prod-db.sql*

# If the DB is not sanitized, do sanitization here.
ddev drush -y sql:sanitize

# Enable dev modules.
ddev drush -y en stage_file_proxy devel

ddev drush cr
ddev drush uli
